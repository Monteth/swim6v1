package com.ct8.monteth.swim6v1.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.icu.util.Calendar
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.ct8.monteth.swim6v1.R
import com.ct8.monteth.swim6v1.Services.CustomRecorder
import com.ct8.monteth.swim6v1.Services.getFilePatch
import com.ct8.monteth.swim6v1.Services.saveMetadata
import com.ct8.monteth.swim6v1.toast
import kotlinx.android.synthetic.main.activity_record.*
import java.text.SimpleDateFormat
import java.util.*


class RecordActivity : AppCompatActivity() {

    val PREFS = "com.ct8.monteth.swim6v1.prefs"
    val RECORD_ID = "recordId"
    val RECORD_DEFAULT = "Record_"
    lateinit var customRecorder: CustomRecorder
    var canSaveFile = false
    var recordName = ""

    @SuppressLint("CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record)

//        TODO: automatyczne wycinanie dłuższych fragmentów ciszy


        initButtons()
        askUserPermissions()
        initRecorder()

    }

    private fun initRecorder() {
        val sharedPreferences = getSharedPreferences(PREFS, Context.MODE_PRIVATE)
        val recordId = sharedPreferences.getInt(RECORD_ID, 0)
        val editor = sharedPreferences.edit()
        editor.putInt(RECORD_ID, recordId+1)
        editor.apply()
        recordName = RECORD_DEFAULT + recordId
        customRecorder = CustomRecorder(recordName + ".wav")
    }

    @SuppressLint("SimpleDateFormat")
    private fun initButtons() {
        acRecord_btRecordStop.setOnClickListener {
            if (customRecorder.isRecording) {
                customRecorder.stopRecording()
                acRecord_tvElapsedTime.text = "RECORDING STOPPED"
                canSaveFile = true
                //enable saveFile button
            } else {
                customRecorder.startRecording()
                acRecord_tvElapsedTime.text = "RECORDING..."
            }
        }

        acRecord_btSaveList.setOnClickListener {
            if (canSaveFile) {
                //changeState

                if (acRecord_etSurname.text.isNotEmpty() &&
                        acRecord_etName.text.isNotEmpty() &&
                        acRecord_etTitle.text.isNotEmpty() &&
                        acRecord_etDesc.text.isNotEmpty()){

                    val sdf = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
                    val date = sdf.format(Date()).toString()
                    customRecorder.saveWavFile(recordName)
                    canSaveFile = false

                    val player = MediaPlayer()
                    player.setDataSource(getFilePatch() + "/" + recordName + ".wav")
                    player.prepare()
                    toast(applicationContext, player.duration.toString())

                    saveMetadata(filename = recordName,
                            surname = acRecord_etSurname.text.toString(),
                            name = acRecord_etName.text.toString(),
                            date = date,
                            recordLength = ((player.duration.toFloat()) / 1000).toInt(),
                            title = acRecord_etTitle.text.toString(),
                            note = acRecord_etDesc.text.toString())
                    initRecorder()
                    toast(applicationContext, "Zapisano!")
                }else {
                    toast(applicationContext, "Najpierw wypełnij pola!!")
                }

            } else {
                if (!customRecorder.isRecording) {
                    startActivity(Intent(applicationContext, ViewRecordsActivity::class.java))
                }
            }
        }

        acRecord_btPause.setOnClickListener {
            if (!customRecorder.isPaused) {
                customRecorder.pauseRecording()
                acRecord_tvElapsedTime.text = "PAUSED"
            } else {
                customRecorder.unpauseRecording()
                acRecord_tvElapsedTime.text = "RECORDING..."
            }
        }
    }

    private fun askUserPermissions() {
        if (ContextCompat.checkSelfPermission(applicationContext,
                        Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    MY_PERMISSIONS_REQUEST_RECORD_AUDIO)
        }
    }

    private val MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 1234
}

