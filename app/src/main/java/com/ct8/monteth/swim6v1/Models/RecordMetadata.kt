package com.ct8.monteth.swim6v1.Models

data class RecordMetadata(
                       val surname:String,
                       val name: String,
                       val date: String,
                       val recordLength: Int,
                       val title: String,
                       val note: String)
