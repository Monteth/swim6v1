package com.ct8.monteth.swim6v1

import android.content.Context
import android.widget.Toast
import kotlin.math.abs

fun toast(context: Context, text: String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}

fun isNoisy(data: ByteArray): Boolean{
    return data.any { byte: Byte -> abs(byte.toInt()) > 100 }
}