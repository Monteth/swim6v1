package com.ct8.monteth.swim6v1.Activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.ct8.monteth.swim6v1.Adapters.RecordsAdapter
import com.ct8.monteth.swim6v1.R
import com.ct8.monteth.swim6v1.Services.*
import com.ct8.monteth.swim6v1.toast
import kotlinx.android.synthetic.main.activity_view_records.*
import java.io.File


class ViewRecordsActivity : AppCompatActivity() {
    lateinit var wavFiles: List<File>
    lateinit var jsonFiles: List<File>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_records)

        val directory = File(getFilePatch())

        wavFiles = directory.listFiles().filter { it.name.endsWith(".wav") }
        jsonFiles = directory.listFiles().filter { it.name.endsWith(".json") }


        val wavAbsFilenames = directory.listFiles()
                .filter {
                    it.name
                            .endsWith(".wav")
                }
                .map {
                    it.absoluteFile.toString()
                            .removeSuffix(".wav")
                }

        val metadata = jsonFiles
                .map { readMetadata(it.absolutePath) }
                .toTypedArray()
        acViewRec_recycler.apply {
            itemAnimator = DefaultItemAnimator()
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = RecordsAdapter(wavAbsFilenames)
        }

        acViewRec_btMerge.setOnClickListener {
            val pos1 = Integer.parseInt(editTextMerge1.text.toString())
            val pos2 = Integer.parseInt(editTextMerge2.text.toString())
            if (pos1 < wavAbsFilenames.size && pos2 < wavAbsFilenames.size){
                val f1 = wavAbsFilenames[pos1]
                val f2 = wavAbsFilenames[pos2]
                WavMerger.combineWaveFiles("$f1.wav", "$f2.wav")
                combineJsonFiles("$f1.json", "$f2.json")
                toast(applicationContext, "Połączony $pos2 do $pos1")
                acViewRec_recycler.adapter.notifyDataSetChanged()
                acViewRec_recycler.adapter.notifyItemRemoved(pos2)
            }else {
                toast(applicationContext, "Nie można połączyć nagrań na tych pozycjach")
            }
        }
    }


}
