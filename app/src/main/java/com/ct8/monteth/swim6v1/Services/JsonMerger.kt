package com.ct8.monteth.swim6v1.Services

import org.json.JSONObject
import java.io.File


fun combineJsonFiles(file1: String, file2: String) {

    val bufferedReader = File(file1).bufferedReader()
    val inputString = bufferedReader.use { it.readText() }

    val json = JSONObject(inputString)

    val out = File(file1.replace(".json", "Merged.json"))
    out.writeText(json.toString())

    File(file1).delete()
    File(file2).delete()
}
