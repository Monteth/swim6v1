package com.ct8.monteth.swim6v1.Services

import android.content.Context.MODE_PRIVATE
import android.os.Environment
import com.ct8.monteth.swim6v1.Models.RecordMetadata
import org.json.JSONObject
import android.widget.Toast
import java.io.*


private val AUDIO_RECORDER_FOLDER = "AudioRecorder"

fun getFilePatch(): String {
    val filepath = Environment.getExternalStorageDirectory().path
    val file = File(filepath, AUDIO_RECORDER_FOLDER)

    if (!file.exists()) {
        file.mkdirs()
    }

    return file.absolutePath
}

val JSON_TITLE = "title"
val JSON_NOTE = "note"
val JSON_LENGTH = "length"
val JSON_DATE = "date"
val JSON_AUTH_NAME = "author_name"
val JSON_AUTH_SURNAME = "author_surname"

fun saveMetadata(filename: String,
                 surname:String,
                 name: String,
                 date: String,
                 recordLength: Int,
                 title: String,
                 note: String){

    val json = JSONObject().apply {
        put(JSON_TITLE, title)
        put(JSON_NOTE, note)
        put(JSON_LENGTH, recordLength)
        put(JSON_DATE, date)
        put(JSON_AUTH_NAME, name)
        put(JSON_AUTH_SURNAME, surname)
    }

    try {
        val file = File(getFilePatch() + "/" + filename + ".json")
        file.writeText(json.toString())
    } catch (e: Exception) {
        print(e.stackTrace)
    }
}


fun readMetadata(filename: String): RecordMetadata {
    val bufferedReader = File(filename).bufferedReader()
    val inputString = bufferedReader.use { it.readText() }

    val json = JSONObject(inputString)

    return RecordMetadata(
            json.getString(JSON_AUTH_SURNAME),
            json.getString(JSON_AUTH_NAME),
            json.getString(JSON_DATE),
            json.getInt(JSON_LENGTH),
            json.getString(JSON_TITLE),
            json.getString(JSON_NOTE)
    )
}