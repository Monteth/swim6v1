package com.ct8.monteth.swim6v1.Adapters

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.ct8.monteth.swim6v1.R
import com.ct8.monteth.swim6v1.Services.WavMerger
import com.ct8.monteth.swim6v1.Services.readMetadata
import java.io.File

class RecordsAdapter(var files: List<String>) :
        RecyclerView.Adapter<RecordsAdapter.ViewHolder>() {
    var metadata = files.map { readMetadata("$it.json") }
    private val selected = mutableListOf<Int>()

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.recordRow_tvTitle)
        val description: TextView = view.findViewById(R.id.recordRow_tvDesc)
        val length: TextView = view.findViewById(R.id.recordRow_tvLength)
        val author: TextView = view.findViewById(R.id.recordRow_tvAuthor)
        val date: TextView = view.findViewById(R.id.recordRow_tvDate)
        val playButton: ImageButton = view.findViewById(R.id.recordRow_btPlayStop)
    }


    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecordsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.record_row, parent, false)

        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.apply {
            title.text = metadata[position].title
            description.text = metadata[position].note
            length.text = metadata[position].recordLength.toString() + "s"
            author.text = metadata[position].name + " " + metadata[position].surname
            date.text = metadata[position].date
            playButton.setOnClickListener {
                val player = MediaPlayer()
                player.setDataSource(files[position] + ".wav")
                player.prepare()
                player.start()
            }
            view.setOnClickListener {
                when {
                    selected.contains(position) -> selected.remove(position)
                    selected.size == 2 -> {
                        WavMerger.combineWaveFiles(files[selected[0]] + ".wav", files[selected[1]] + ".wav")
                        selected.clear()
                    }
                    else -> selected.add(position)
                }
                //select or unselect
                //check if 2 selected
                //merge if so
            }
            view.setOnLongClickListener {
                val wavFile = File(files[position] + ".wav")
                val jsonFile = File(files[position] + ".json")
                wavFile.delete()
                jsonFile.delete()
                files = files.toMutableList().drop(position).toList()
                val temp = files.toMutableList()
                temp.removeAt(position)
                files = temp
                metadata = files.map { readMetadata("$it.json") }
                super.notifyDataSetChanged()
                true
            }
        }
    }


    override fun getItemCount() = files.size
}